<!-- @AUTHOR SIVA KUMAR ATHINARAM @DATE: 08/31/2017 -->
(function() {
	angular
			.module('HealthInsurance', [ 'ui.router' ])
			.constant('APP_NAME', 'Health Insurance Quote')
			.config(function($stateProvider, $urlRouterProvider) {
				$stateProvider.state('home', {
					url : '/',
					templateUrl : 'home.html',
					controller : 'HealthQuoteController'
				}).state('404', {
					url : '/404',
					templateUrl : 'error404.html'
				});
				// if the route not found it will route to below route
				$urlRouterProvider.otherwise("404");
			})
			.controller('HealthQuoteController',['$scope', '$log','$http',function($scope, $log, $http) {
						// Initializing the form with empty values on the every page load
						$scope.healthform = {
							'name' : '',
							'gender' : '',
							'age' : '',
							'hypertension' : '',
							'bloodpressure' : '',
							'bloodsugar' : '',
							'overweight' : '',
							'smoking' : '',
							'alcohol' : '',
							'dailyexcercise' : '',
							'drugs' : ''
						}
						//Form submit call this function to calculate the premium
						$scope.calculatepremium = function(form) {
							
							$http({
							    method: 'POST',
							    url: 'http://localhost:8091/quote/calculate',
							    data: form,
							    headers: {'Content-Type': 'application/json'}
							}).then(function(response){
								
								var response=response.data;
								
								if(response.statusCode===200 && response.successMessage==='success'){
									
									var premiumAmount=response.calculatedPremium
									// Logging the final calculated premium amount
									$log.info($scope.premium = 'Health Insurance Premium for Mr.'+ form.name +': Rs. '+ Math.round(premiumAmount));
									// Passing the final calculated premium anount to html file to display
									$scope.premium = 'Health Insurance Premium for Mr.'+ form.name +': Rs. '+ Math.round(premiumAmount);
								}
								else if(response.statusCode===500 && response.successMessage==='failure')
									{
									$scope.errorMessage = response.errorDescription;
									}
							});
							
							
							
						}

					}]);
						
})();
