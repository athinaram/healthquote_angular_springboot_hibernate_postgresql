package com.example.demo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@AUTHOR SIVA KUMAR ATHINARAM @DATE: 08/31/2017 

@RestController
@RequestMapping("/quote")
public class PremiumController {

	@Autowired
	private PremiumService premiumservice;
	@Autowired
	private Result result;
	

	@ResponseBody
	@Produces({"application/json"})
	@Consumes({"application/json"})
	@RequestMapping(value="/calculate",method=RequestMethod.POST)
	 public  Result calculatePremium(@RequestBody InsurancePremiumQuote insurancepremiumquote){
		double premiumAmount;
		try{
			 premiumAmount=premiumservice.calculatePremium(insurancepremiumquote);
		}
		catch(Exception ex){
			System.out.println("Not able to process your request at this time. Please try again later (or) contact: 123-456-7891");
			result.setCalculatedPremium(0.0);
			result.setStatusCode(500);
			result.setSuccessMessage("failure");
			return result;
		}
		 result.setCalculatedPremium(premiumAmount);
		 result.setStatusCode(200);
		 result.setSuccessMessage("success");
		return result;
	 }
	
}
