package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PremiumRepository extends JpaRepository<InsurancePremiumQuote, Integer>{

}
