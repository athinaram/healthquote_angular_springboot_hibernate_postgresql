package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//@AUTHOR SIVA KUMAR ATHINARAM @DATE: 08/31/2017 

@Service
public class PremiumService {

	@Autowired
	private PremiumService premiumService;

	@Autowired
	private PremiumRepository premiumRepository;

	public static double BASE_PREMIUM = 5000;

	public static double BASE_PERCENTAGE = 0.1;

	private static double BASE_PREMIUM_18AND25 = BASE_PREMIUM + (BASE_PREMIUM * BASE_PERCENTAGE);

	private static double BASE_PREMIUM_25AND30 = BASE_PREMIUM_18AND25 + (BASE_PREMIUM_18AND25 * BASE_PERCENTAGE);

	private static double BASE_PREMIUM_30AND35 = BASE_PREMIUM_25AND30 + (BASE_PREMIUM_25AND30 * BASE_PERCENTAGE);

	private static double BASE_PREMIUM_35AND40 = BASE_PREMIUM_30AND35 + (BASE_PREMIUM_30AND35 * BASE_PERCENTAGE);

	private static double BASE_PREMIUM_40_PLUS = 0.0;

	public PremiumService() {
		super();
	}

	public double calculatePremium(InsurancePremiumQuote insurancepremiumquote) {

		double premiumAmount;

		Integer age = insurancepremiumquote.getAge();

		premiumAmount = premiumService.calculatePremiumForAge(age);

		if (insurancepremiumquote.getGender().equalsIgnoreCase("M")) {
			premiumAmount = premiumService.calculatePremiumForMale(premiumAmount);
		}

		premiumAmount = premiumService.calculatePremiumForCurrentHealthConditions(insurancepremiumquote, premiumAmount);

		premiumAmount = premiumService.calculatePremiumForHabits(insurancepremiumquote, premiumAmount);

		insurancepremiumquote.setPremiumamount(premiumAmount);

		premiumRepository.save(insurancepremiumquote);

		return premiumAmount;

	}

	public double calculatePremiumForAge(int age) {

		if (age < 18) {
			return BASE_PREMIUM;
		} else if (age >= 18 && age < 25) {
			return BASE_PREMIUM_18AND25;
		} else if (age >= 25 && age < 30) {
			return BASE_PREMIUM_25AND30;
		} else if (age >= 30 && age < 35) {
			return BASE_PREMIUM_30AND35;
		} else if (age >= 35 && age < 40) {
			return BASE_PREMIUM_35AND40;
		} else {
			int currentAge = age - 40;
			int ageQuotient = (int) Math.floor(currentAge / 5);
			int ageReminder = currentAge % 5;
			if (ageReminder > 0) {
				ageQuotient = ageQuotient + 1;
			}
			for (int i = 1; i <= ageQuotient; i++) {
				BASE_PREMIUM_40_PLUS = BASE_PREMIUM_40_PLUS + (0.2);
			}
			return BASE_PREMIUM_40_PLUS;
		}
	}

	private double calculatePremiumForMale(double premiumAmount) {

		return premiumAmount + (premiumAmount * (0.02));
	}

	private double calculatePremiumForCurrentHealthConditions(InsurancePremiumQuote insurancepremiumquote,
			double premiumAmount) {

		int count = 0;

		if (insurancepremiumquote.getHypertension().equalsIgnoreCase("Yes")) {
			count++;
		}
		if (insurancepremiumquote.getBloodpressure().equalsIgnoreCase("Yes")) {
			count++;
		}
		if (insurancepremiumquote.getBloodsugar().equalsIgnoreCase("Yes")) {
			count++;
		}
		if (insurancepremiumquote.getOverweight().equalsIgnoreCase("Yes")) {
			count++;
		}

		premiumAmount = premiumAmount + (premiumAmount * (count * 0.01));

		return premiumAmount;

	}

	private double calculatePremiumForHabits(InsurancePremiumQuote insurancepremiumquote, double premiumAmount) {

		int count = 0;

		if (insurancepremiumquote.getSmoking().equalsIgnoreCase("Yes")) {
			count++;
		}
		if (insurancepremiumquote.getAlcohol().equalsIgnoreCase("Yes")) {
			count++;
		}
		if (insurancepremiumquote.getDrugs().equalsIgnoreCase("Yes")) {
			count++;
		}

		if (insurancepremiumquote.getDailyexcercise().equalsIgnoreCase("Yes")) {
			if (count > 0) {
				count = count - 1;
			} else {
				count = count + 1;
			}

		}
		return premiumAmount + (premiumAmount * (count * (3 / 100)));

	}

}
