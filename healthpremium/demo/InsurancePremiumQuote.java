package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

//@AUTHOR SIVA KUMAR ATHINARAM @DATE: 08/31/2017 

@Entity
@Table(name="premiums")
public class InsurancePremiumQuote {
	
	@Id
	@SequenceGenerator(name = "seqGenerator", sequenceName = "poke_key")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGenerator")
	@Column(name="QUOTEID")
	private Integer quoteId;
	@Column(name="NAME")
	private String name;
	@Column(name="GENDER")
	private String gender;
	@Column(name="AGE")
	private Integer age;
	@Column(name="HYPERTENSION")
	private String hypertension;
	@Column(name="BLOOD_PRESSURE")
	private String bloodpressure;
	@Column(name="BLOOD_SUGAR")
	private String bloodsugar;
	@Column(name="OVERWEIGHT")
	private String overweight;
	@Column(name="SMOKING")
	private String smoking;
	@Column(name="ALCOHOL")
	private String alcohol;
	@Column(name="DAILY_EXCERCISE")
	private String dailyexcercise;
	@Column(name="DRUGS")
	private String drugs;
	@Column(name="PREMIUM_AMOUNT")
	private double premiumamount;
	
	public InsurancePremiumQuote() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getQuoteId() {
		return quoteId;
	}


	public void setQuoteId(Integer quoteId) {
		this.quoteId = quoteId;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getBloodpressure() {
		return bloodpressure;
	}
	public void setBloodpressure(String bloodpressure) {
		this.bloodpressure = bloodpressure;
	}
	public String getBloodsugar() {
		return bloodsugar;
	}
	public void setBloodsugar(String bloodsugar) {
		this.bloodsugar = bloodsugar;
	}
	public String getOverweight() {
		return overweight;
	}
	public void setOverweight(String overweight) {
		this.overweight = overweight;
	}
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getDailyexcercise() {
		return dailyexcercise;
	}
	public void setDailyexcercise(String dailyexcercise) {
		this.dailyexcercise = dailyexcercise;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}

	public double getPremiumamount() {
		return premiumamount;
	}

	public void setPremiumamount(double premiumamount) {
		this.premiumamount = premiumamount;
	}


	
	

}
