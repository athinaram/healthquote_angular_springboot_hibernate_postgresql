package com.example.demo;

import org.springframework.stereotype.Component;

//@AUTHOR SIVA KUMAR ATHINARAM @DATE: 08/31/2017 

@Component
public class Result {
	
	private double calculatedPremium;
	
	private String successMessage;
	
	private int statusCode;
	
	private String errorDescription;

	
	public Result() {
		super();
	}

	public double getCalculatedPremium() {
		return calculatedPremium;
	}

	public void setCalculatedPremium(double calculatedPremium) {
		this.calculatedPremium = calculatedPremium;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String string) {
		this.errorDescription = string;
	}
	
	
	

}
