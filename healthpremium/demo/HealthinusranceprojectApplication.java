package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@AUTHOR SIVA KUMAR ATHINARAM @DATE: 08/31/2017 
@SpringBootApplication
public class HealthinusranceprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(HealthinusranceprojectApplication.class, args);
	}
}
